 
## requirement of laravel 5.7

PHP 7.2.2
MYSQL 5.7.19

## Installation

set config .env your database & make sure artisan key:generate

```bash
composer install/update
php artisan migrate
php artisan db:seed
```
 
Admin Login:

Email: admin@example.com

password : admin

-------------------------------------------------
Customer Login:

Email: [ see the users list ]

password : customer 