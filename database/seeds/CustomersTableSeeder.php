<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('customers')->delete();
        
        \DB::table('customers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'name' => 'Andy Leo',
                'phone' => '089636845137',
                'address' => 'Jl gapura menteng',
                'created_at' => '2019-04-02 23:09:26',
                'updated_at' => '2019-04-02 23:09:26',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 3,
                'name' => 'Ade Iskandar',
                'phone' => '0948599545',
                'address' => 'Jl gapura menteng',
                'created_at' => '2019-04-02 23:17:16',
                'updated_at' => '2019-04-02 23:17:16',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 4,
                'name' => 'Hari Yanto',
                'phone' => '0948599545',
                'address' => 'Pamulang',
                'created_at' => '2019-04-03 06:22:45',
                'updated_at' => '2019-04-03 06:22:45',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}