<?php

use Illuminate\Database\Seeder;

class OrderDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_details')->delete();
        
        \DB::table('order_details')->insert(array (
            0 => 
            array (
                'id' => 1,
                'order_id' => 1,
                'product_id' => 1,
                'created_at' => '2019-04-02 23:09:57',
                'updated_at' => '2019-04-02 23:09:57',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'order_id' => 1,
                'product_id' => 2,
                'created_at' => '2019-04-02 23:09:58',
                'updated_at' => '2019-04-02 23:09:58',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'order_id' => 2,
                'product_id' => 3,
                'created_at' => '2019-04-02 23:17:39',
                'updated_at' => '2019-04-02 23:17:39',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'order_id' => 2,
                'product_id' => 4,
                'created_at' => '2019-04-02 23:17:39',
                'updated_at' => '2019-04-02 23:17:39',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'order_id' => 3,
                'product_id' => 5,
                'created_at' => '2019-04-02 23:18:52',
                'updated_at' => '2019-04-02 23:18:52',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'order_id' => 4,
                'product_id' => 6,
                'created_at' => '2019-04-02 23:20:08',
                'updated_at' => '2019-04-02 23:20:08',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'order_id' => 5,
                'product_id' => 7,
                'created_at' => '2019-04-03 06:23:06',
                'updated_at' => '2019-04-03 06:23:06',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'order_id' => 5,
                'product_id' => 8,
                'created_at' => '2019-04-03 06:23:07',
                'updated_at' => '2019-04-03 06:23:07',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}