<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$2GsI9HiwW7HF2jK4Mb544eue4.nSD9jjTuP0h5uU9j/FAcBE3Z3WC',
                'role_id' => 1,
                'remember_token' => 'woO41WcbY0MpJWju00FBIFwa5MNsDhzQCEcQkDMs16B6Ge4hq4vIax9oMoh3',
                'created_at' => '2019-03-05 06:35:23',
                'updated_at' => '2019-03-05 06:35:23',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Andy Leo',
                'email' => 'andyleo1554246565@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$XsOqHhEA0Eb4VG8PiUNZvOwCVg2wafJ0V8rmv6xppyUPmllL2D9Ae',
                'role_id' => 2,
                'remember_token' => 'XYrl5dZ7jp6ZvBd1H451wWgAJ7HcUBLxh7jmC7T6Ck47epbJYHinpTJg53lg',
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ade Iskandar',
                'email' => 'adeiskandar1554247036@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$4.T/WekFktVgRdb87LR2DuwjHlqFgCCd9plyeCX3cXudvOY2EvQwS',
                'role_id' => 2,
                'remember_token' => 'i7iAbDWAeos8s9RFtnhHNM4U0V9wOwCr3UunrdqHSMVXWov9eTMGKjneyCFW',
                'created_at' => '2019-04-02 23:17:16',
                'updated_at' => '2019-04-02 23:17:16',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Hari Yanto',
                'email' => 'hariyanto1554247364@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$baeC2adx6xaHPWBvH6w/VeaKSd2T/FTKU/ULT20uIPqt1hcl.vUm2',
                'role_id' => 2,
                'remember_token' => 'PUkjP2KI46K92kzBVRg5dbXhOT51LoR68gQXfNOjPlmOlvHeFEGfHG6I6Uv9',
                'created_at' => '2019-04-03 06:22:44',
                'updated_at' => '2019-04-03 06:22:44',
            ),
        ));
        
        
    }
}