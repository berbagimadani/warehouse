<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('orders')->delete();
        
        \DB::table('orders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'customer_id' => 1,
                'warehouse_id' => 3,
                'invoice' => '1554246598',
                'status' => '2',
                'created_at' => '2019-04-02 23:09:57',
                'updated_at' => '2019-04-02 23:18:20',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'customer_id' => 2,
                'warehouse_id' => 2,
                'invoice' => '1554247060',
                'status' => '1',
                'created_at' => '2019-04-02 23:17:39',
                'updated_at' => '2019-04-02 23:17:39',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'customer_id' => 1,
                'warehouse_id' => 1,
                'invoice' => '1554247133',
                'status' => '1',
                'created_at' => '2019-04-02 23:18:52',
                'updated_at' => '2019-04-02 23:18:52',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'customer_id' => 1,
                'warehouse_id' => 1,
                'invoice' => '1554247209',
                'status' => '1',
                'created_at' => '2019-04-02 23:20:08',
                'updated_at' => '2019-04-02 23:20:08',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'customer_id' => 3,
                'warehouse_id' => 3,
                'invoice' => '1554247387',
                'status' => '1',
                'created_at' => '2019-04-03 06:23:06',
                'updated_at' => '2019-04-03 06:23:06',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}