<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
       /* \DB::table('users')->delete();
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'admin@example.com',
                'password' => Hash::make('admin'),
                'name' => 'Admin', 
                'remember_token' => 'z0wuIoabta5LEY0ZuWSLpYxl4V5PURhTCYRLt73Us08FBJA5nYSpDliZowxE',
                'role_id' => 1,
                'email_verified_at' => NULL,
                'created_at' => '2019-03-05 06:35:23',
                'updated_at' => '2019-03-05 06:35:23', 
            ), 
        ));


        \DB::table('warehouses')->delete();
        
        \DB::table('warehouses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'location' => 'Jakarta',
                'space' => '10 x 10 meter',
                'price' => '10000000',  
            ),
            1 => 
            array (
                'id' => 2,
                'location' => 'Bandung',
                'space' => '10 x 10 meter',
                'price' => '20000000',  
            ),
            2 => 
            array (
                'id' => 3,
                'location' => 'Makasar',
                'space' => '10 x 10 meter',
                'price' => '30000000',  
            ),
        )); */

        $this->call(WarehousesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderDetailsTableSeeder::class);
    }
}
