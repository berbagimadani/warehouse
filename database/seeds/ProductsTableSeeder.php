<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'customer_id' => 1,
                'order_id' => 1,
                'name' => 'TV',
                'type' => 'Elektro',
                'weight' => '100 KG',
                'created_at' => '2019-04-02 23:09:38',
                'updated_at' => '2019-04-02 23:09:57',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            1 => 
            array (
                'id' => 2,
                'customer_id' => 1,
                'order_id' => 1,
                'name' => 'Mobil',
                'type' => 'Otomotif',
                'weight' => '100 KG',
                'created_at' => '2019-04-02 23:09:51',
                'updated_at' => '2019-04-02 23:09:58',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            2 => 
            array (
                'id' => 3,
                'customer_id' => 2,
                'order_id' => 2,
                'name' => 'Kuda',
                'type' => 'Binatang',
                'weight' => '100 KG',
                'created_at' => '2019-04-02 23:17:27',
                'updated_at' => '2019-04-02 23:17:39',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            3 => 
            array (
                'id' => 4,
                'customer_id' => 2,
                'order_id' => 2,
                'name' => 'Kucing',
                'type' => 'Binatang',
                'weight' => '100 KG',
                'created_at' => '2019-04-02 23:17:35',
                'updated_at' => '2019-04-02 23:17:40',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            4 => 
            array (
                'id' => 5,
                'customer_id' => 1,
                'order_id' => 3,
                'name' => 'Rumah',
                'type' => 'Property',
                'weight' => '100 KG',
                'created_at' => '2019-04-02 23:18:49',
                'updated_at' => '2019-04-02 23:18:52',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            5 => 
            array (
                'id' => 6,
                'customer_id' => 1,
                'order_id' => 4,
                'name' => 'TV',
                'type' => 'Elektro',
                'weight' => '11',
                'created_at' => '2019-04-02 23:20:05',
                'updated_at' => '2019-04-02 23:20:08',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            6 => 
            array (
                'id' => 7,
                'customer_id' => 3,
                'order_id' => 5,
                'name' => 'Mobil',
                'type' => 'Otomotif',
                'weight' => '100 KG',
                'created_at' => '2019-04-03 06:22:52',
                'updated_at' => '2019-04-03 06:23:07',
                'deleted_at' => NULL,
                'status' => '2',
            ),
            7 => 
            array (
                'id' => 8,
                'customer_id' => 3,
                'order_id' => 5,
                'name' => 'TV',
                'type' => 'Elektro',
                'weight' => '11',
                'created_at' => '2019-04-03 06:23:01',
                'updated_at' => '2019-04-03 06:23:07',
                'deleted_at' => NULL,
                'status' => '2',
            ),
        ));
        
        
    }
}