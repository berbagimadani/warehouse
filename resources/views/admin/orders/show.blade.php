@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Invoice Order #{{ $order->invoice }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/orders') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a> 
                        
                        <br/>
                        <br/>
                        <div>
                            <label>Customer</label> : {{ $order->customer->name }} <br>
                            <label>Phone</label> : {{ $order->customer->phone }} <br>
                            <label>Warehouse</label> : {{ $order->warehouse->location }} - {{ $order->warehouse->space }} <br>
                            <label>Price</label> : {{ $order->warehouse->price }}
                            <br>
                            @if($order->status==1)
                            @if(Auth::user()->role_id==1)
                            <form method="POST" action="{{ url('admin/orders' . '/' . $order->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <input type="hidden" name="status" value="2">
                                <button type="submit" class="btn btn-primary btn-sm" title="" onclick="return confirm(&quot;Confirm Finish?&quot;)"> Finish Out </button>
                            </form>
                            @endif
                            @else
                                <label>Finished</label>
                            @endif
                            <br>
                            <br>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Weight</th>
                                </tr>
                                
                                @foreach($order->order_details as $row)
                                <tr>
                                    <td>{{ @$row->product->name }}</td>
                                    <td>{{ @$row->product->type }}</td>
                                    <td>{{ @$row->product->weight }}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
