@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        
        <div class="col-md-9">

           
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">Input Barang</div>
                                <div class="card-body">
                                  <form method="POST" action="{{ url('/admin/products') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <input type="hidden" value="{{$customer->id}}" name="customer">  
                                    @include ('admin.products.form', ['formMode' => 'create'])

                                </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">Data Customer</div>
                                <div class="card-body">
                                    <label>Name</label> : {{$customer->name}} <br>
                                    <label>Phone Number</label> :  {{$customer->phone}} <br>
                                    <label>Address</label> : {{$customer->address}} <br>
                                </div>
                            </div>
                             
                        </div>
                    </div>
                </div>
            </div> 
          
            <div class="row" style="padding-top: 10px;">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Data Barang</div>
                        <div class="card-body">
                            
                             <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Type</th><th>Weight</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->type }}</td><td>{{ $item->weight }}</td>
                                        <td>

                                            <form method="POST" action="{{ url('/admin/products' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <input type="hidden" value="{{$customer->id}}" name="customer">   
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 10px;">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('/admin/orders') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <div class="card">
                        <div class="card-header">Target Warehouse</div>
                        <div class="card-body"> 
                            @if(isset($warehouses))
                            <select class="form-control" name="warehouse_id">
                                @foreach($warehouses as $row)
                                <option value="{{ $row->id }}">
                                    {{ $row->location }} - {{ $row->space }} ({{ $row->price}})
                                </option>
                                @endforeach
                            </select> 
                            @endif
                        </div>
                    </div>
                    <div class="card"> 
                        @if($products->count() >= 1)
                        <div class="card-body"> 
                            <input type="hidden" name="customer_id" value="{{$customer->id}}">
                            <button type="submit" class="btn btn-lg btn-primary float-right">Complete Order</button> 
                            </form>
                        </div>
                        @endif
                    </div>
                    </form>

                </div>
            </div>  

        </div>
    </div>
</div>
@endsection
