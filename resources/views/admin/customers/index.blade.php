@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12"> 
                <form method="POST" action="{{ url('/admin/customers') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">Add Customer</div>
                                <div class="card-body">
                                    @if ($errors->any())
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    @endif 
                                    
                                        {{ csrf_field() }}

                                        @include ('admin.customers.form', ['formMode' => 'create'])
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            
                        </div>
                    </div>
                </form>
                
                </div> 
            </div>

            <h3 style="margin:0 auto; width: 10%">OR</h3>

            <div class="card" style="margin-top: 10px;">
                
                <div class="card-header">Existing Customer</div> 

                    <!--<div class="float-right" style="padding-top: 10px; padding-left: 10px;">
                    <form method="GET" action="{{ url('/admin/customers') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 " role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    </div>-->
                    
                    <div class="table-responsive"><br/>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th><th>Phone</th><th>Address</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customers as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td><td>{{ $item->phone }}</td><td>{{ $item->address }}</td>
                                    <td>
                                        <a href="{{ url('/admin/product/' . $item->id) }}" title="Create Order"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Create Order</button></a>
                                        <a href="{{ url('/admin/customers/' . $item->id . '/edit') }}" title="Edit customer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                        <form method="POST" action="{{ url('/admin/customers' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete customer" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $customers->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
