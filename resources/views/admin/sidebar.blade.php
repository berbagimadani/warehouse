<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/home') }}">
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/orders') }}">
                        Orders
                    </a>
                </li> 

                @if(Auth::user()->role_id==1)
                <li>
                    <a href="{{ url('/admin/customers') }}">
                        Input Product/Barang
                    </a>
                </li> 
                <hr>
                <h6>Mater Data</h6> 
                <li>
                    <a href="{{ url('/admin/users') }}">
                        Users
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/warehouse') }}">
                        Warehouse
                    </a>
                </li>
                @endif
            </ul> 
        </div>
    </div>
</div>
