<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\OrderDetail;
use App\Customer;
use Auth;

class Order extends Model
{
 

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['invoice', 'customer_id', 'warehouse_id', 'status'];

    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\warehouse');
    }

     public function scopeRole($query)
    {
        if(Auth::user()->role_id==1){
            return null;
        }
        if(Auth::user()->role_id==2){
            $customer = Customer::whereUserId(Auth::user()->id)->first();
            return $query->whereCustomerId($customer->id);
        }
    }
}
