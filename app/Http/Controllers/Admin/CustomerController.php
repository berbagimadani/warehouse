<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Customer;
use App\Warehouse;
use App\User;

use Illuminate\Http\Request; 
use DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $customers = Customer::latest()->paginate($perPage);
        } else {
            $customers = Customer::latest()->paginate($perPage);
        }

        $warehouses = Warehouse::get();

        return view('admin.customers.index', compact('customers', 'warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $this->validate($request, Customer::$rules); 

        // Start transaction! 
        // untuk multiple insert lebih bagus semua menggunakan transaction

        DB::beginTransaction();
        try {
            
            $user = new User();
            $user->name = $request->name;
            $user->email = Str::slug($request->name, '').time().'@mail.com';
            $user->role_id = 2;
            $user->password = Hash::make('customer');
            $user->save();

        } catch(ValidationException $e) { 
            DB::rollback();
            return $e->getErrors();
        } catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try { 
          
            $requestData['user_id'] = $user->id;
            $customer = Customer::create($requestData);

        } catch(ValidationException $e) { 
            DB::rollback();
            return $e->getErrors();
        } catch(\Exception $e) {
            DB::rollback();
            throw $e;
        } 
        DB::commit();

        return redirect('admin/product/'.$customer->id);

        //return redirect('admin/customers')->with('flash_message', 'Customer added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        return view('admin.customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        return view('admin.customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $customer = Customer::findOrFail($id);
        $customer->update($requestData);

        return redirect('admin/customers')->with('flash_message', 'Customer updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        return redirect('admin/customers')->with('flash_message', 'Customer deleted!');
    }
}
