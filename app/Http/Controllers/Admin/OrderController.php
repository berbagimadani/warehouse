<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\Customer;
use App\warehouse;
use App\Product;
use App\OrderDetail;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        

        if (!empty($keyword)) {
            $orders = Order::role()->with('customer')
                ->where('invoice', 'LIKE', "%$keyword%") 
                ->orWhere('customer_id', 'LIKE', "%$keyword%") 
                ->whereStatus(1)
                ->latest()
                ->paginate($perPage);
        } else {
            $orders = Order::role()->latest()->paginate($perPage);
        } 

        return view('Admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('Admin.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $products = Product::whereCustomerId($request->customer_id)->whereNull('order_id')->get();
        $requestData['invoice'] =  str_pad(time() + 1, 6, "0", STR_PAD_LEFT);
        $requestData['status']  = 1;
        $order = Order::create($requestData);

        $data = array();
        foreach ($products as $key => $value) {
            $od = new OrderDetail();
            $od->order_id = $order->id;
            $od->product_id = $value->id;
            $od->save();

            $product = Product::find($value->id);
            $product->order_id = $order->id;
            $product->status = 2;
            $product->save();
        } 

        return redirect('admin/orders')->with('flash_message', 'Order added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $order = Order::with('order_details')->findOrFail($id);  

        //return $order->order_details;
        return view('Admin.orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);

        return view('Admin.orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $order = Order::findOrFail($id);
        $order->update($requestData);
        $orders = OrderDetail::whereOrderId($id)->get();
         foreach ($orders as $key => $value) {
            $product = Product::find($value->product_id);
            //$product->status = 3;
            $product->order_id = $order->id;
            $product->save();
        } 

        return redirect('admin/orders')->with('flash_message', 'Order updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Order::destroy($id);

        return redirect('admin/orders')->with('flash_message', 'Order deleted!');
    }
}
