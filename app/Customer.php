<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'phone', 'address'];

    public static $rules = [
        'name' => 'required|min:3',
        'phone'  => 'required|min:3', 
        'address'  => 'required|min:5'
    ];

    
}
